#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdbool.h>
#include "modification.h"

//procedure de vigenere
void vigenere(bool chiffrage, wchar_t messa[]) {
	wchar_t charac;
	wchar_t buf;
	wchar_t cle[100];
	do
	{
		buf = getwchar();
	} while (buf != '\n' && buf != EOF);
	wprintf(L"Quelle clé utiliser ?\n");
	fgetws(cle,sizeof cle,stdin);
	remplirMessage(cle);
	supprimerAccents(cle);
	for(int i=0;i < wcslen(messa);i++) {
		if (messa[i] == ' ') {
		} else {
		charac = messa[i] - 'a';
		if (chiffrage)
			charac = charac + (cle[i%wcslen(cle)] - 'a');
		else
			charac = charac - (cle[i%wcslen(cle)] - 'a');
			if (charac < 0)
				charac = charac + 26;
		charac = charac % 26;
		messa[i] = charac + 'a';
		}
	}
	wprintf(L"Voici le message chiffré/dechiffré, il a été stocké dans le fichier resultat.txt\n");
	wprintf(L"%ls\n", messa);
}


void chiffrageVigenere(wchar_t messa[]) {
	vigenere(true, messa);
}

void dechiffrageVigenere(wchar_t messa[]) {
	vigenere(false, messa);
}
