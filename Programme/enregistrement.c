#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdbool.h>

//procedure qui ecrit le message dans un fichier txt
void ecritureFichier(wchar_t messa[]) {
	FILE * fichier;
	fichier = fopen("resultat.txt","w+");
	if (fichier) {
		fprintf(fichier, "%ls\n", messa);
	} else {
		wprintf(L"Erreur à l'ouverture du fichier");
	}
}
