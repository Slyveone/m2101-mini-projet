#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdbool.h>

//procedure cesar
void cesar(bool chiffrage, wchar_t messa[]) {
	//création et demande de la clé de décalage
	int clef;
	wchar_t chara;
	wprintf(L"De combien voulez vous faire le décalage ? \n");
	wscanf(L"%d", &clef);
	//boucle de décalage
	for (int i=0;i < wcslen(messa);i++) {
		//vérification de présence d'espace
		if (messa[i] == ' ') {
		} else {
		chara = messa [i] - 'a';
		//vérification chiffrage = true alors chiffrage sinon déchiffrage
		if (chiffrage)
			chara = chara + clef;
		else
			chara = chara - clef;
			if (chara < 0)
				chara = chara + 26;
		chara = chara % 26;
		messa[i] = chara + 'a';
		}
	}
	//affichage du message
	wprintf(L"Voici le message chiffré/dechiffré, il a été stocké dans le fichier resultat.txt\n");
	wprintf(L"%ls\n", messa);
}

//procedure pour chiffrer en cesar
void chiffrageCesar(wchar_t messa[]) {
	cesar(true, messa);
}

//procedure pour dechiffrer en cesar
void dechiffrageCesar(wchar_t messa[]) {
	cesar(false, messa);
}

