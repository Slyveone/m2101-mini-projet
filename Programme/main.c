
#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdbool.h>
#include "cesar.h"
#include "vigenere.h"
#include "modification.h"
#include "enregistrement.h"
#include "choix.h"

void main() {
	struct lconv *loc;
	setlocale(LC_ALL, "");
	loc = localeconv();
	//message a chiffrer/dechiffrer
	wchar_t messa[100];
	wprintf(L"Veuillez entrer le message à chiffrer ou déchiffrer : \n");
	fgetws(messa, 100, stdin);
	//supprimer majuscules accents et caractères "entrée"
	remplirMessage(messa);
	supprimerAccents(messa);
	//taille du message à chiffrer
	int taillemessage = wcslen(messa);
	//vérification de la présence de caractères invalides
	if (caractereInvalide(messa) == 1) {
		wprintf(L"Présence de caractère invalide\n");
	}
	//chiffrage ou dechiffrage du message
	else choixChiffrage(messa);
	//ecriture du message dans un fichier 
	ecritureFichier(messa);
}
